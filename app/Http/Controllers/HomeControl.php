<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Barang;
use Illuminate\Support\Facades\Redirect;

class HomeControl extends Controller
{
    /**
     * Display the barang Datas.
     */
    public function homePage()
    {
        $months = [
            0 => ['label' => 'Januari', 'value' => 1],
            1 => ['label' => 'Februari', 'value' => 2],
            2 => ['label' => 'Maret', 'value' => 3],
            3 => ['label' => 'April', 'value' => 4],
            4 => ['label' => 'Mei', 'value' => 5],
            5 => ['label' => 'Juni', 'value' => 6],
            6 => ['label' => 'Juli', 'value' => 7],
            7 => ['label' => 'Agustus', 'value' => 8],
            8 => ['label' => 'September', 'value' => 9],
            9 => ['label' => 'Oktober', 'value' => 10],
            10 => ['label' => 'November', 'value' => 11],
            11 => ['label' => 'December', 'value' => 12],
        ];

        $barang = Barang::orderBy('tahun')->get();

        return Inertia::render('Dashboard', [
            'months' => $months,
            'datas' => $barang,
        ]);
    }

    /**
     * Create the data barang.
     */
    public function ceateBarang(Request $request)
    {
        // dd($request);

        $createBarang = Barang::create([
            'tahun' => $request->tahun,
            'bulan' => $request->bulan,
            'nama_barang' => $request->nama_barang,
            'stok_barang' => $request->stok_barang,
            'terjual_barang' => $request->terjual_barang,
        ]);

        return Redirect::route('dashboard');
    }

    /**
     * Update the data barang.
     */
    public function editBarang(Request $request)
    {
        
        $updateBarang = Barang::find($request->id);

        $updateBarang->update([
            'tahun' => $request->tahun,
            'bulan' => $request->bulan,
            'nama_barang' => $request->nama_barang,
            'stok_barang' => $request->stok_barang,
            'terjual_barang' => $request->terjual_barang,
        ]);

        return Redirect::route('dashboard');
    }

    /**
     * Delete the data barang.
     */
    public function deleteBarang(Request $request)
    {
        
        $deleteBarang = Barang::find($request->id);

        $deleteBarang->delete();

        return Redirect::route('dashboard');
    }
}
