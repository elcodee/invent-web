<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Barang;
use App\Models\Prediksi;
use Illuminate\Support\Facades\Redirect;
use DB;

class PrediksiControl extends Controller
{
    /**
     * Display the barang Datas.
     */
    public function prediksiPage()
    {
        $months = [
            0 => ['label' => 'Januari', 'value' => 1],
            1 => ['label' => 'Februari', 'value' => 2],
            2 => ['label' => 'Maret', 'value' => 3],
            3 => ['label' => 'April', 'value' => 4],
            4 => ['label' => 'Mei', 'value' => 5],
            5 => ['label' => 'Juni', 'value' => 6],
            6 => ['label' => 'Juli', 'value' => 7],
            7 => ['label' => 'Agustus', 'value' => 8],
            8 => ['label' => 'September', 'value' => 9],
            9 => ['label' => 'Oktober', 'value' => 10],
            10 => ['label' => 'November', 'value' => 11],
            11 => ['label' => 'Desember', 'value' => 12],
        ];

        $barang = Barang::get();
        $deliverNewDatas = Prediksi::get();

        // dd(count($deliverNe/wDatas));

        return Inertia::render('Prediksi', [
            'months' => $months,
            'datas' => $barang,
            'new_datas' => $deliverNewDatas,
            'show' => count($deliverNewDatas) ? true : false,
        ]);
    }

     /**
     * Counting Prediction barang.
     */
    public function countPrediction(Request $request)
    {
        // Insert Data To DB
        foreach ($request->dataBarang as $val => $data) {
            if($val > 0){
                $createBarang = Prediksi::create([
                    'tahun' => $request->tahun,
                    'bulan' => $data['bulan'],
                    'nama_barang' => $data['nama_barang'],
                    'terjual_current_month' => $data['terjual_barang'],
                    'terjual_past_month' => $request->dataBarang[$val - 1]['terjual_barang'],
                    
                    'alpha' => $request->alpha,
                ]);
            } else {
                $createBarang = Prediksi::create([
                    'tahun' => $request->tahun,
                    'bulan' => $data['bulan'],
                    'nama_barang' => $data['nama_barang'],
                    'terjual_current_month' => $data['terjual_barang'],
                    'alpha' => $request->alpha,
                ]);
            }
        }

        // Then Count Prediction Data After Insert Data To DB
        $getPredictionDatas = Prediksi::get();

        foreach ($getPredictionDatas as $key => $value) {
            $findCountingData = Prediksi::find($value->id);

            if($key > 0){
                // if($value->alpha === '0.2'){
                    $count = ((0.2 * (int)$value->terjual_current_month) + (1 - 0.2) * (int)$value->terjual_past_month);
                    $findCountingData->update([
                        'prediksi' => $count,
                    ]);
                // }

                // if($value->alpha === '0.8'){
                //     $count = ((0.8 * (int)$value->terjual_current_month) + (1 - 0.8) * (int)$value->terjual_past_month);
                //     $findCountingData->update([
                //         'prediksi' => $count,
                //     ]);
                // }
            }
        }

        // Then Count MAD Data After Insert Data To DB
        $getMadDatas = Prediksi::get();

        foreach ($getMadDatas as $key => $value) {
            $findCountingmadData = Prediksi::find($value->id);

            if($key > 0){
                $mad = ((int)$value->prediksi - (int)$value->terjual_current_month) / 11;
                $findCountingmadData->update([
                    // 'mad' => abs(number_format((float)$mad, 2, '.', '')),
                    'mad' => abs(number_format($mad, 2, '.', '')),
                ]);
            }
        }

        // Then Count MSE Data After Insert Data To DB
        $getMseDatas = Prediksi::get();

        foreach ($getMseDatas as $key => $value) {
            $findCountingmseData = Prediksi::find($value->id);

            if($key > 0){
                $mse = ((int)$value->prediksi - (int)$value->terjual_current_month) ** 2 / 11;
                $findCountingmseData->update([
                    // 'mse' => abs(number_format((float)$mse, 2, '.', '')),
                    'mse' => abs(number_format($mse, 2, '.', '')),
                ]);
            }
        }

        return Redirect::route('prediksi');
    }

    /**
     * Delete the user's account.
     */
    public function deletePredictionData()
    {
        Prediksi::truncate();  
        return Redirect::route('prediksi');
        // return Redirect::to('/prediksi');
    }
}
