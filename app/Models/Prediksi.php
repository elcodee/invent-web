<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prediksi extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'tahun',
        'bulan',
        'nama_barang',
        'terjual_current_month',
        'terjual_past_month',
        'prediksi',
        'mad',
        'mse',
        'alpha',
    ];
}
