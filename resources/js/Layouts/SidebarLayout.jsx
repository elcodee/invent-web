import {
    Avatar,
    Box,
    Flex,
    Icon,
    Text,
    Link,
    Image,
    Button,
    Heading,
    Stack,
    VStack,
    Drawer,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    DrawerContent,
    IconButton,
    useDisclosure,
    DrawerOverlay,
    useColorModeValue
} from "@chakra-ui/react"
// Here we have used react-icons package for the icons
import { AiOutlineTeam, AiOutlineHome, AiOutlineSnippets } from "react-icons/ai"
import { FiMenu } from "react-icons/fi"

export default function SidebarLayout({ user, children }) {
    const { isOpen, onClose, onOpen } = useDisclosure();

    return (
        <Box
            as="section"
            bg={useColorModeValue("gray.50", "gray.700")}
            minH="100vh"
        >
            <SidebarContent display={{ base: "none", md: "unset" }} />
            <Drawer isOpen={isOpen} onClose={onClose} placement="left">
                <DrawerOverlay />
                <DrawerContent>
                    <SidebarContent w="full" borderRight="none" />
                </DrawerContent>
            </Drawer>
            <Box ml={{ base: 0, md: 60 }} transition=".3s ease">
                <Flex
                    as="header"
                    align="center"
                    w="full"
                    px="4"
                    d={{ base: "flex", md: "none" }}
                    borderBottomWidth="1px"
                    borderColor={useColorModeValue("inherit", "gray.700")}
                    bg={useColorModeValue("#5DADEC", "gray.800")}
                    justify={{ base: "space-between", md: "flex-end" }}
                    boxShadow="lg"
                    h="14"
                >
                    <IconButton
                        aria-label="Menu"
                        display={{ base: "inline-flex", md: "none" }}
                        onClick={onOpen}
                        icon={<FiMenu />}
                        size="md"
                    />

                    <Flex align="center">
                        <Icon as={AiOutlineSnippets} h={8} w={8} color="white" />
                    </Flex>
                </Flex>

                <Box
                    as="main"
                    p={14}
                    minH="30rem"
                    bg={useColorModeValue("#f0f8ff", "gray.800")}
                >
                    <Stack
                        direction={{ base: "column", sm: "row" }}
                        alignItems="center"
                        justifyContent="center"
                        h="100%"
                    >
                        <Stack spacing={8}>
                            {children}
                        </Stack>

                    </Stack>
                </Box>
            </Box>
        </Box>
    )
}

const SidebarContent = ({ ...props }) => (
    <Box
        as="nav"
        pos="fixed"
        top="0"
        left="0"
        zIndex="sticky"
        h="full"
        // pb="10"
        overflowX="hidden"
        overflowY="auto"
        color="white"
        bg={useColorModeValue("#5DADEC", "gray.800")}
        borderColor={useColorModeValue("inherit", "gray.700")}
        borderRightWidth="1px"
        w="60"
        {...props}
    >
        <VStack h="full" w="full" alignItems="flex-start" justify="space-between">
            <Box w="full">
                <Flex px="4" py="5" align="center">
                    <Icon as={AiOutlineSnippets} h={8} w={8} />
                    <Text
                        fontSize="2xl"
                        ml="2"
                        color={useColorModeValue("brand.500", "white")}
                        fontWeight="semibold"
                    >
                        Inventory Web
                    </Text>
                </Flex>
                <Flex
                    direction="column"
                    as="nav"
                    fontSize="md"
                    color="gray.600"
                    aria-label="Main Navigation"
                >
                    {/* <NavItem icon={AiOutlineHome}>Home</NavItem> */}
                </Flex>
            </Box>

            {/* <Flex px="4" py="5" mt={10} justify="center" alignItems="center">
                <Menu>
                    <MenuButton
                        as={Button}
                        size={"sm"}
                        rounded={"full"}
                        variant={"link"}
                        cursor={"pointer"}
                        _hover={{ textDecoration: "none" }}
                    >
                        <Avatar
                            size={"sm"}
                            name="Ahmad"
                            src="https://avatars2.githubusercontent.com/u/37842853?v=4"
                        />
                    </MenuButton>
                    <MenuList fontSize={17} zIndex={5555}>
                        <MenuItem as={Link} to="#">
                            My profile
                        </MenuItem>
                        <MenuItem as={Link} to="#">
                            Change password
                        </MenuItem>
                        <MenuItem>Logout</MenuItem>
                    </MenuList>
                </Menu>
            </Flex> */}
        </VStack>
    </Box>
)

const NavItem = props => {
    const color = useColorModeValue("#5DADEC", "gray.300")

    const { icon, children } = props
    return (
        <Flex
            align="center"
            px="4"
            py="3"
            cursor="pointer"
            role="group"
            fontWeight="semibold"
            transition=".15s ease"
            color={useColorModeValue("white", "gray.400")}
            _hover={{
                bg: useColorModeValue("gray.100", "gray.900"),
                color: useColorModeValue("gray.900", "gray.200")
            }}
        >
            {icon && (
                <Icon
                    mx="2"
                    boxSize="4"
                    _groupHover={{
                        color: color
                    }}
                    as={icon}
                />
            )}
            {children}
        </Flex>
    )
}
