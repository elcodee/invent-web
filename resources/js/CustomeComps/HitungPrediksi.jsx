import {
    AlertDialog,
    AlertDialogBody,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogContent,
    AlertDialogOverlay,
    AlertDialogCloseButton,
    Button,
    useDisclosure,
    FormControl,
    FormLabel,
    Select,
    Alert,
    AlertIcon
} from '@chakra-ui/react'
import { useForm } from '@inertiajs/react';
import React, { useState } from 'react'

export default function HitungPrediksi({ datas, tahun }) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const cancelRef = React.useRef();
    const { data, setData, post, processing, errors, reset } = useForm({
        dataBarang: datas,
        tahun: tahun,
        alpha: "",
    });

    const submit = () => {
        post(route('prediksi.hitung'));
        onClose();
    };

    return (
        <>
            <Button mr={2} mt={4} backgroundColor={"#5DADEC"} color="white" disabled={datas.length === 0} size='md' onClick={datas.length === 0 ? '' : onOpen}>
                Hitung
            </Button>

            <AlertDialog
                motionPreset='slideInBottom'
                leastDestructiveRef={cancelRef}
                onClose={onClose}
                isOpen={isOpen}
                isCentered
                closeOnOverlayClick={false}
            >
                <AlertDialogOverlay />

                <AlertDialogContent>
                    <AlertDialogHeader>Hitung Prediksi</AlertDialogHeader>
                    <AlertDialogCloseButton />
                    <AlertDialogBody>
                        {
                            !data.alpha &&
                            <Alert status='warning' variant='top-accent' mb={4}>
                                <AlertIcon />
                                Pilih Alpha {`(α)`} Untuk Memulai Perhitungan Prediksi
                            </Alert>
                        }

                        <ul>
                            <li><b>Nama Barang :</b> {datas[0]?.nama_barang}</li>
                            {
                                data?.alpha && <li><b>Alpha  :</b> {`α | ${data?.alpha}`}</li>
                            }
                        </ul>

                        <FormControl mt={6}>
                            <FormLabel>Alpha {`(α)`}</FormLabel>
                            <Select variant='filled' placeholder='Pilih...' name='tahun' onChange={(e) => setData('alpha', e.target.value)}>
                                {
                                    // [{ label: "α | 0,2", value: "0.2" }, { label: "α | 0,8", value: "0.8" }].map((a, b) => {
                                    [{ label: "α | 0,2", value: "0.2" }].map((a, b) => {
                                        return <option value={a.value}>{a.label}</option>
                                    })
                                }
                            </Select>
                        </FormControl>
                    </AlertDialogBody>
                    <AlertDialogFooter>
                        <Button ref={cancelRef} color="red" onClick={onClose}>
                            Batal
                        </Button>
                        {
                            data?.alpha &&
                            <Button backgroundColor={"#5DADEC"} color="white" ml={3} disabled={!data?.alpha} onClick={() => submit()}>
                                Hitung
                            </Button>
                        }
                    </AlertDialogFooter>
                </AlertDialogContent>
            </AlertDialog>
        </>
    )
}