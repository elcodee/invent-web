import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Button,
    FormControl,
    FormLabel,
    Select,
    Input,
} from '@chakra-ui/react'
import { useForm } from '@inertiajs/react';
import Swal from "sweetalert2";

export default function DeleteModal({ datas }) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const {
        data,
        setData,
        delete: destroy,
        processing,
        reset,
        errors,
    } = useForm({
        id: datas.id,
        tahun: datas.tahun,
        bulan: datas.bulan,
        nama_barang: datas.nama_barang,
        stok_barang: datas.stok_barang,
        terjual_barang: datas.terjual_barang,
    });

    const deleteUser = () => {
        destroy(route('delete.barang'), {
            preserveScroll: true,
            onSuccess: () => onClose(),
            onFinish: () => reset(),
        });

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
        })

        Toast.fire({
            icon: 'success',
            title: 'Data Berhasil Di Hapus !'
        })
    };

    return (
        <>
            <Button mr={2} onClick={onOpen} backgroundColor="#E53E3E" color="white" size='xs'>
                Hapus
            </Button>

            <Modal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Hapus Data ?</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <FormControl>
                            <FormLabel>Nama Barang</FormLabel>
                            <Input variant='filled' disabled defaultValue={datas.nama_barang} type='text' name='nama_barang' placeholder='nama barang' />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Tahun / Bulan</FormLabel>
                            <Input variant='filled' disabled defaultValue={`${datas.tahun} / ${datas.bulan}`} type='text' name='nama_barang' placeholder='tahun' />
                        </FormControl>


                        <FormControl mt={4}>
                            <FormLabel>Stok Barang</FormLabel>
                            <Input variant='filled' disabled defaultValue={datas.stok_barang} type='text' name='stok_barang' placeholder='0' />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Terjual</FormLabel>
                            <Input variant='filled' disabled defaultValue={datas.terjual_barang} type='text' name='terjual_barang' placeholder='0' />
                        </FormControl>
                    </ModalBody>

                    <ModalFooter>
                        <Button variant='ghost' colorScheme='red' mr={3} onClick={onClose}>
                            Tutup
                        </Button>
                        <Button backgroundColor="#5DADEC" onClick={deleteUser}>Hapus</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}