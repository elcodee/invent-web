import * as React from "react"
import {
    Container,
    Box,
    chakra,
    Flex,
    Divider,
    TableContainer,
    Table,
    Thead,
    Tbody,
    Th,
    Tr,
    Td,
    useColorModeValue
} from "@chakra-ui/react"
import Swal from "sweetalert2";

const TableYearlyHasValue = ({ data }) => {

    const handleEmptyData = (msg, t) => {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
        })

        Toast.fire({
            icon: t,
            title: msg
        })
    }

    React.useEffect(() => {
        if (data.length === 0) {
            handleEmptyData("Gagal Error syatem !", "error");
        } else {
            handleEmptyData("Success", "success");
        }
    }, [data.length]);

    return (
        <Container minW="full" py={2} px={2}>
            <Box
                border="1px solid"
                borderColor="gray.400"
                rounded="md"
                boxShadow="lg"
                overflow="hidden"
            >
                <Flex justify="left" p={5}>
                    <chakra.h3 fontSize="xl" fontWeight="bold" textAlign="center">
                        {data[0]?.nama_barang}
                    </chakra.h3>
                </Flex>
                <Divider />
                <TableContainer>
                    <Table size="md">
                        <Thead>
                            <Tr fontWeight="900">
                                <Th>Bulan</Th>
                                <Th>Penjualan</Th>
                                <Th>Prediksi</Th>
                                <Th>MAD</Th>
                                <Th>MSE</Th>
                                <Th>Alpha {`(α)`}</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {
                                data ? data.map((a, b) => {
                                    return (
                                        <Tr>
                                            <Td fontSize="sm">{a.bulan}</Td>
                                            <Td fontSize="sm">{a.terjual_barang ? a.terjual_barang : a.terjual_current_month}</Td>
                                            <Td fontSize="sm">{a?.prediksi ? a.prediksi : "0"}</Td>
                                            <Td fontSize="sm">{a?.mad ? a.mad : "0"}</Td>
                                            <Td fontSize="sm">{a?.mse ? a.mse : "0"}</Td>
                                            <Td fontSize="sm">{a?.alpha ? a.alpha : "0"}</Td>
                                        </Tr>
                                    )
                                }) : null
                            }

                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Container>
    )
}

export default TableYearlyHasValue
