import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Button,
    FormControl,
    FormLabel,
    Select,
    Input,
} from '@chakra-ui/react'
import { useForm } from '@inertiajs/react';
import Swal from "sweetalert2";

export default function TambahModal({ months }) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const { data, setData, post, processing, errors, reset } = useForm({
        id: '',
        tahun: '',
        bulan: '',
        nama_barang: '',
        stok_barang: '',
        terjual_barang: '',
    });

    const submit = () => {
        post(route('create.barang'));
        onClose();

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
        })

        Toast.fire({
            icon: 'success',
            title: 'Data Berhasil Di Tambahkan !'
        })
    };

    return (
        <>
            <Button mr={2} onClick={onOpen} backgroundColor="#5DADEC" color="white" size='md'>
                Tambah Barang
            </Button>

            <Modal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Tambah Data</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <FormControl>
                            <FormLabel>Tahun</FormLabel>
                            <Select variant='filled' placeholder='Pilih Tahun...' name='tahun' defaultValue={data.tahun} onChange={(e) => setData('tahun', e.target.value)}>
                                {
                                    ['2019', '2020', '2021', '2022', '2023'].map((a, b) => {
                                        return <option value={a}>{a}</option>
                                    })
                                }
                            </Select>
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Bulan</FormLabel>
                            <Select variant='filled' placeholder='Pilih Bulan...' name='bulan' defaultValue={data.bulan} onChange={(e) => setData('bulan', e.target.value)}>
                                {
                                    months && months.map((a, b) => {
                                        return (
                                            <option value={a.label}>{a.label}</option>
                                        )
                                    })
                                }
                            </Select>
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Nama Barang</FormLabel>
                            <Input variant='filled' defaultValue={data.nama_barang} type='text' name='nama_barang' placeholder='nama barang' onChange={(e) => setData('nama_barang', e.target.value)} />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Stok Barang</FormLabel>
                            <Input variant='filled' defaultValue={data.stok_barang} type='text' name='stok_barang' placeholder='0' onChange={(e) => setData('stok_barang', e.target.value)} />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Terjual</FormLabel>
                            <Input variant='filled' defaultValue={data.terjual_barang} type='text' name='terjual_barang' placeholder='0' onChange={(e) => setData('terjual_barang', e.target.value)} />
                        </FormControl>
                    </ModalBody>

                    <ModalFooter>
                        <Button variant='ghost' colorScheme='red' mr={3} onClick={onClose}>
                            Tutup
                        </Button>
                        <Button backgroundColor="#5DADEC" onClick={submit}>Simpan</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}