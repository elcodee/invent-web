import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, useForm } from '@inertiajs/react';
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Heading,
    FormLabel,
    FormControl,
    Select,
    Text,
    AbsoluteCenter,
    Box,
    Spinner,
    Button,
    Flex,
    Spacer,
    Card,
    CardBody,
    Radio,
    RadioGroup,
    Stack,
} from '@chakra-ui/react'
import { useEffect, useState } from 'react';
import EditModal from '@/CustomeComps/ModalEdit';
import DeleteModal from '@/CustomeComps/ModalDelete';
import TambahModal from '@/CustomeComps/ModalTambah';
import TableYearly from '@/CustomeComps/TableYearly';
import TableYearlyHasValue from '@/CustomeComps/TableYearlyHasValue';
import Swal from "sweetalert2";
import HitungPrediksi from '@/CustomeComps/HitungPrediksi';

export default function Prediksi({ auth, months, datas, new_datas, show }) {
    const [initDatas, setInitDatas] = useState(new_datas ? new_datas : []);
    const [selectedBarang, setSelectedBarang] = useState("");
    const [selectedYear, setSelectedYear] = useState("");
    const [showFiled, setShowField] = useState({
        tahun: false,
        bulan: false,
        tBulan: false,
        tTahun: false,
        tTahunValue: false,
    });
    const { delete: destroy, } = useForm();

    const handleLoading = (msg) => {
        let timerInterval
        Swal.fire({
            title: msg,
            timer: 1000,
            timerProgressBar: true,
            allowOutsideClick: false,
            didOpen: () => {
                Swal.showLoading();
            },
            willClose: () => {
                clearInterval(timerInterval);
            }
        })
    }

    const handleYearChange = (val) => {
        handleLoading('Getting Data...');

        let yearData = datas.filter((el) => {
            return el.tahun === val &&
                el.nama_barang === selectedBarang;
        });

        setTimeout(() => {
            setSelectedYear(val);
            setInitDatas(yearData);
            setShowField({
                bulan: false,
                tahun: true,
                tTahun: true,
            });
        }, 1300);
    };

    const handleReset = () => {
        handleLoading('Reseting form...');

        setTimeout(() => {
            setSelectedBarang("");
            setInitDatas([]);
            setShowField({
                bulan: false,
                tahun: false,
                tTahun: false,
                tBulan: false,
                tTahunValue: false,
            });

            destroy(route('prediksi.truncate'));
        }, 1300);
    }

    console.log(show, showFiled, new_datas);

    useEffect(() => {

        if (show === true) {
            setShowField({
                ...showFiled,
                tahun: false,
                tTahun: false,
                tTahunValue: show,
            });
        }

    }, [selectedBarang, initDatas, show])
    return (
        <AuthenticatedLayout
            user={auth.user}
            // header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Prediksi</h2>}
        >
            <Head title="Dashboard" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">

                    <Flex minWidth='max-content' alignItems='center' gap='2'>
                        <Box p='2'>
                            <Heading
                                as="h3"
                                fontSize="1.5rem"
                                fontWeight="bold"
                                textAlign="left"
                                mb={{ base: '4', md: '2' }}
                                pb={4}
                                borderColor="gray.300"
                            >
                                Hitung Prediksi
                            </Heading>
                        </Box>
                        <Spacer />
                    </Flex>

                    <Card backgroundColor="#ecf6ff">
                        <CardBody>
                            <FormControl mb={4}>
                                <FormLabel>Pilih Barang</FormLabel>
                                <Select variant='filled' placeholder='Pilih...' value={selectedBarang} onChange={(e) => {
                                    setSelectedBarang(e.target.value);
                                    setShowField({
                                        bulan: false,
                                        tahun: true,
                                    });
                                }}>
                                    {
                                        datas && datas.map((a, b) => {
                                            if(a.bulan === 'Januari'){
                                                return (
                                                    <option value={a.nama_barang}>{a.nama_barang}</option>
                                                )
                                            }
                                        })
                                    }
                                </Select>
                            </FormControl>

                            {/* <FormControl mb={4}>
                                <FormLabel>Tipe</FormLabel>
                                <RadioGroup defaultValue='0' onChange={(e) => handleTypeChange(e)}>
                                    <Stack spacing={5} direction='row'>
                                        <Radio colorScheme='red' value='perbulan'>
                                            Perbulan
                                        </Radio>
                                        <Radio colorScheme='green' value='pertahun'>
                                            Pertahun
                                        </Radio>
                                    </Stack>
                                </RadioGroup>
                            </FormControl> */}

                            {
                                showFiled.tahun &&
                                <>
                                    <FormControl mb={4}>
                                        <FormLabel>Tahun</FormLabel>
                                        <Select variant='filled' placeholder='Pilih...' name='tahun' onChange={(e) => handleYearChange(e.target.value)}>
                                            {
                                                ['2019', '2020', '2021', '2022', '2023'].map((a, b) => {
                                                    return <option value={a}>{a}</option>
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                </>
                            }

                            {
                                showFiled.tTahun &&
                                <>
                                    <TableYearly data={initDatas} />

                                    <Flex minWidth='max-content' alignItems='center' gap='2'>
                                        <Spacer />
                                        <Button mr={2} mt={4} colorScheme='red' size='md' onClick={handleReset}>
                                            Reset
                                        </Button>
                                        <HitungPrediksi datas={initDatas} tahun={selectedYear} />
                                    </Flex>
                                </>
                            }

                            {
                                show &&
                                <>
                                    <TableYearlyHasValue data={new_datas} />

                                    <Flex minWidth='max-content' alignItems='center' gap='2'>
                                        <Spacer />
                                        <Button mr={2} mt={4} colorScheme='red' size='md' onClick={handleReset}>
                                            Reset
                                        </Button>
                                        <HitungPrediksi datas={initDatas} tahun={selectedYear} />
                                    </Flex>
                                </>
                            }
                        </CardBody>
                    </Card>

                </div>
            </div>
        </AuthenticatedLayout>
    );
}
