import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Heading,
    FormLabel,
    FormControl,
    Select,
    Text,
    AbsoluteCenter,
    Box,
    Spinner,
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionIcon,
    AccordionPanel,
    Flex,
    Spacer,
    Button,
} from '@chakra-ui/react'
import { useEffect, useState } from 'react';
import EditModal from '@/CustomeComps/ModalEdit';
import DeleteModal from '@/CustomeComps/ModalDelete';
import TambahModal from '@/CustomeComps/ModalTambah';

export default function Dashboard({ auth, months, datas }) {
    const [initDatas, setInitDatas] = useState([]);
    const [newinitData, setNewInitDatas] = useState([]);
    const [loading, setLoading] = useState(false);

    const monthFilters = (val) => {
        setInitDatas(datas);
        setLoading(true);

        let newDatas = datas.filter((el) => el.bulan === val);

        setTimeout(() => {
            setInitDatas(newDatas);
            setLoading(false);
        }, 1000);
    };

    const yearFilters = (val) => {
        setInitDatas([]);
        setLoading(true);

        let yearDatas = initDatas.filter((el) => el.tahun === val);

        setTimeout(() => {
            setInitDatas(yearDatas);
            setLoading(false);
        }, 1000);
    };

    const resetFilters = () => {
        setLoading(true);

        setTimeout(() => {
            setInitDatas(datas);
            setLoading(false);
        }, 1000);
    };

    useEffect(() => {
        setInitDatas(datas);
    }, [datas])
    return (
        <AuthenticatedLayout
            user={auth.user}
            // header={<h2 className="font-semibold text-xl text-white-800 leading-tight">Dashboard</h2>}
        >
            <Head title="Dashboard" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">

                    <Flex minWidth='max-content' alignItems='center' gap='2'>
                        <Box p='2'>
                            <Heading
                                as="h3"
                                fontSize="1.5rem"
                                fontWeight="bold"
                                textAlign="left"
                                mb={{ base: '4', md: '2' }}
                                pb={4}
                                borderColor="gray.300"
                            >
                                Data Barang
                            </Heading>
                        </Box>
                        <Spacer />
                        <TambahModal months={months} />
                    </Flex>

                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg p-4" style={{ backgroundColor: "#f0f8ff"}}>

                        <Accordion allowToggle mb={8} backgroundColor="#ffffff">
                            <AccordionItem>
                                <h2>
                                    <AccordionButton _expanded={{ bg: '#5DADEC', color: 'white' }}>
                                        <Box as="span" flex='1' textAlign='left'>
                                            FILTER DATA
                                        </Box>
                                        <AccordionIcon />
                                    </AccordionButton>
                                </h2>
                                <AccordionPanel>
                                    <FormControl mb={4}>
                                        <FormLabel>Tahun</FormLabel>
                                        <Select placeholder='Pilih...' onChange={(e) => yearFilters(e.target.value)}>
                                            {
                                                ['2019', '2020', '2021', '2022', '2023'].map((a, b) => {
                                                    return <option value={a}>{a}</option>
                                                })
                                            }
                                        </Select>
                                    </FormControl>

                                    <FormControl mb={4}>
                                        <FormLabel>Bulan</FormLabel>
                                        <Select placeholder='Pilih...' onChange={(e) => monthFilters(e.target.value)}>
                                            {
                                                months && months.map((a, b) => {
                                                    return (
                                                        <option value={a.label}>{a.label}</option>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>

                                    <Flex minWidth='max-content' alignItems='center' gap='2'>
                                        <Spacer />
                                        <Button mr={2} onClick={resetFilters} colorScheme='red' size='sm'>
                                            Reset Filter
                                        </Button>
                                    </Flex>
                                </AccordionPanel>
                            </AccordionItem>
                        </Accordion>

                        <TableContainer>
                            <Table variant='simple'>
                                <TableCaption>{initDatas.length ? `Total Data :  ${initDatas.length}` :
                                    <Text fontSize='15px' color='tomato'>
                                        Data is Empty
                                    </Text>
                                }</TableCaption>
                                <Thead>
                                    <Tr>
                                        <Th>No</Th>
                                        <Th>Tahun</Th>
                                        <Th>Bulan</Th>
                                        <Th>Nama Barang</Th>
                                        <Th>Stok Barang</Th>
                                        <Th>Terjual</Th>
                                        <Th>Aksi</Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                    {
                                        loading &&
                                        <Tr position='relative'>
                                            <Box justifyContent="center" h='100px'>
                                                <AbsoluteCenter p='4' color='white' axis='both'>
                                                    <Spinner
                                                        thickness='4px'
                                                        speed='0.65s'
                                                        emptyColor='gray.200'
                                                        color='blue.500'
                                                        size='xl'
                                                    />
                                                </AbsoluteCenter>
                                            </Box>
                                        </Tr>
                                    }

                                    {
                                        initDatas.length >= 0 && initDatas.map((a, b) => {
                                            return (
                                                <Tr>
                                                    <Td>{a.id}</Td>
                                                    <Td>{a.tahun}</Td>
                                                    <Td>{a.bulan}</Td>
                                                    <Td>{a.nama_barang}</Td>
                                                    <Td>{a.stok_barang}</Td>
                                                    <Td>{a.terjual_barang}</Td>
                                                    <Td>
                                                        <EditModal datas={a} months={months} />
                                                        <DeleteModal datas={a} />
                                                    </Td>
                                                </Tr>
                                            )
                                        })
                                    }
                                </Tbody>
                                <Tfoot>
                                    <Tr>
                                        <Th>No</Th>
                                        <Th>Tahun</Th>
                                        <Th>Bulan</Th>
                                        <Th>Nama Barang</Th>
                                        <Th>Stok Barang</Th>
                                        <Th>Terjual</Th>
                                        <Th>Aksi</Th>
                                    </Tr>
                                </Tfoot>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
