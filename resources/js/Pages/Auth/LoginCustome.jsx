import { useEffect, useState } from "react"
import {
    Container,
    FormControl,
    FormLabel,
    Input,
    Stack,
    Button,
    Heading,
    useColorModeValue,
    VStack,
    Center,
    InputGroup,
    InputRightElement,
    Checkbox,
    Link
} from "@chakra-ui/react";
import { useForm } from "@inertiajs/react";
import InputError from "@/Components/InputError";

const SignInCustome = () => {
    const [show, setShow] = useState(false)
    const handleClick = () => setShow(!show)

    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        password: '',
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <Container maxW="7xl" p={{ base: 5, md: 10 }}>
            <Center>
                <Stack spacing={4}>
                    <Stack align="center">
                        <Heading fontSize="2xl">Login to your account</Heading>
                    </Stack>
                    <VStack
                        as="form"
                        boxSize={{ base: "xs", sm: "sm", md: "md" }}
                        h="max-content !important"
                        bg={useColorModeValue("white", "gray.700")}
                        rounded="lg"
                        boxShadow="lg"
                        p={{ base: 5, sm: 10 }}
                        spacing={8}
                    >
                        <VStack spacing={4} w="100%">
                            <FormControl id="email">
                                <FormLabel>Email</FormLabel>
                                <Input
                                    rounded="md"
                                    type="email"
                                    id="email"
                                    name="email"
                                    value={data.email}
                                    className="mt-1 block w-full"
                                    autoComplete="username"
                                    isFocused={true}
                                    onChange={(e) => setData('email', e.target.value)} />
                                <InputError message={errors.email} className="mt-2" />
                            </FormControl>
                            <FormControl id="password">
                                <FormLabel>Password</FormLabel>
                                <InputGroup size="md">
                                    <Input
                                        rounded="md"
                                        type={show ? "text" : "password"}
                                        id="password"
                                        name="password"
                                        value={data.password}
                                        className="mt-1 block w-full"
                                        autoComplete="current-password"
                                        onChange={(e) => setData('password', e.target.value)}
                                    />
                                    <InputRightElement width="4.5rem">
                                        <Button
                                            h="1.75rem"
                                            size="sm"
                                            rounded="md"
                                            color="white"
                                            bg={useColorModeValue("#2dc937", "gray.700")}
                                            _hover={{
                                                bg: useColorModeValue("#88ef8f", "gray.800")
                                            }}
                                            onClick={handleClick}
                                        >
                                            {show ? "Hide" : "Show"}
                                        </Button>
                                    </InputRightElement>
                                </InputGroup>

                                <InputError message={errors.password} className="mt-2" />
                            </FormControl>
                        </VStack>
                        <VStack w="100%">
                            <Button
                                bg="#2dc937"
                                color="white"
                                _hover={{
                                    bg: "#88ef8f"
                                }}
                                rounded="md"
                                w="100%"
                                disabled={processing}
                                onClick={submit}
                            >
                                Login
                            </Button>
                        </VStack>
                    </VStack>
                </Stack>
            </Center>
        </Container>
    )
}

export default SignInCustome
