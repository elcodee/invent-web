import SidebarLayout from '@/Layouts/SidebarLayout';
import { Link, Head } from '@inertiajs/react';
import SignInCustome from './Auth/LoginCustome';

export default function Welcome({ auth, laravelVersion, phpVersion }) {
    return (
        <>
            <Head title="Web Inventory" />

            <SidebarLayout user={auth.user}>
                <SignInCustome />
            </SidebarLayout>
        </>
    );
}
