<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeControl;
use App\Http\Controllers\PrediksiControl;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware('auth')->group(function () {
    // Data Barang
    Route::get('/dashboard', [HomeControl::class, 'homePage'])->name('dashboard');
    Route::post('/create-barang', [HomeControl::class, 'ceateBarang'])->name('create.barang');
    Route::patch('/edit-barang', [HomeControl::class, 'editBarang'])->name('edit.barang');
    Route::delete('/delete-barang', [HomeControl::class, 'deleteBarang'])->name('delete.barang');
    
    // Prediksi
    Route::get('/prediksi', [PrediksiControl::class, 'prediksiPage'])->name('prediksi');
    Route::post('/hitung', [PrediksiControl::class, 'countPrediction'])->name('prediksi.hitung');
    Route::delete('/reset-tbl-prediksi', [PrediksiControl::class, 'deletePredictionData'])->name('prediksi.truncate');

    // User Profile
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
